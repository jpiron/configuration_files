# ~/.bashrc: executed by bash(1) for non-login shells.
# shellcheck disable=SC2148

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# reset the prompt command variable
PROMPT_COMMAND=""

# Add local binaries in path.
export PATH=$PATH:${HOME}/.local/bin/

# Define EDITOR
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/code

##################################################################################
#
# HISTORY STUFFS
#
##################################################################################
# don't put duplicate lines in the history. See bash(1) for more options
HISTCONTROL=ignoreboth
# append to the history file, don't overwrite it
shopt -s histappend
# append history file immedialtly. Usefull across multiple tmux panels.
PROMPT_COMMAND="history -a;${PROMPT_COMMAND}"
HISTSIZE=15000
HISTFILESIZE=15000

##################################################################################
#
# MISC STUFFS
#
##################################################################################
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# for bash rematch to work as in  bash3.1
shopt -s compat31
# to avoid ^C character to be outputed when pressing ctrl-C
stty -echoctl

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

##################################################################################
#
# PROMPT STUFFS
#
##################################################################################
function truncate_pwd {
  new_pwd=$(basename "${PWD}")
  local pwd_max_len=25
  if [ ${#new_pwd} -gt $pwd_max_len ];then
    new_pwd="...${new_pwd: -$pwd_max_len}"
  fi
}

# truncate the pwd before displying the prompt
PROMPT_COMMAND="truncate_pwd; ${PROMPT_COMMAND}"

# Configure git_ps1
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUPSTREAM="auto verbose"

# # Configure kubectl context check
# PROMPT_COMMAND="check_kubectl_ctx; ${PROMPT_COMMAND}"
# Init environment to current kubectl context
export ENVIRONMENT=${ENVIRONMENT:-sandbox}
export K8S_CLUSTER=${K8S_CLUSTER:-f8b331da-2cad-46b0-af8a-7ec0aef9ee20}

PS1='[\[\033[38;5;196m\]$(echo $ENVIRONMENT)\[\033[38;5;47m\]|$(echo ${K8S_CLUSTER})(${K8S_NAMESPACE})|\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[1;33m\]$new_pwd$(__git_ps1 "\[\033[1;34m\](%s)")\[\033[0m\]]\$ '

##################################################################################
#
# TOOLS STUFFS
#
##################################################################################
export PATH=${HOME}/.local/bin:$PATH
export PATH=${HOME}/.local/bin/platform-tools:$PATH
export PATH=$PATH:/usr/local/go/bin

# BEGIN zoxide
eval "$(zoxide init bash)"
# END zoxide

# BEGIN direnv
eval "$(direnv hook bash)"
# END direnv

# BEGIN kubectl
source <(kubectl completion bash)
# END kubectl
alias k='kubectl'
complete -o default -F __start_kubectl k

# BEGIN stern
source <(stern --completion=bash)
# END stern

# BEGIN FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
export FZF_DEFAULT_OPTS="--layout=reverse --header-first --color bg:#222222,preview-bg:#333333"
export FZF_CTRL_T_OPTS="
  --preview 'bat -n --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_CTRL_R_OPTS="
  --preview 'echo {}' --preview-window up:3:hidden:wrap
  --bind 'ctrl-/:toggle-preview'
  --bind 'ctrl-y:execute-silent(echo -n {2..} | pbcopy)+abort'
  --color header:italic
  --header 'Press CTRL-Y to copy command into clipboard'"
# END FZF

# BEGIN TASK
[ -f ~/.task.bash ] && source ~/.task.bash
# END TASK

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

sandbox_sbg

# load ssh agent with keys if not already loaded and create ~/.ssh/ssh_auth_sock symlink to be used
# by tmux
# sa

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

#!/bin/bash

alias ll='ls -lA'
alias la='ls -lA'
alias l='ls -CF'
alias cdig2="dig axfr local | grep -E"
alias cdig='cat ~/.ssh/config | grep -Fi hostname | awk "{print \$2}" | sort | uniq | grep -F'
alias tmr='tmux renamew'
alias tmls='tmux list-sessions'
alias tmks='tmux kill-session -t'
alias tmat='tmux attach -t'
if [ -x /usr/bin/colordiff ]; then
    alias ddiff='colordiff -rU1'
else
    alias ddiff='diff -rU1'
fi
alias clear_ru='rm ~/.local/share/recently-used.xbel &&  touch ~/.local/share/recently-used.xbel'
# alias git='lab'
#alias gitk='gitk --all'

# alias workstation_start='VBoxManage startvm jastec_debian --type headless'
# alias workstation_stop='ssh workstation "sudo poweroff"'

# remove unused images
alias docker_rmi='docker images --filter "dangling=true" -q | xargs --no-run-if-empty docker rmi'
# remove all exited docker container
alias docker_rma='docker ps -a -q -f status=exited | xargs --no-run-if-empty docker rm'
# remove unused volumes.
alias docker_rmv='docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm'

alias h='helm'

##################################################################################
#
# CUSTOM SHORTUCTS
#
##################################################################################
function sa()
{
    # gnome keyring automatically starts an SSH agent and sets the SSH_AUTH_SOCK env var.
    # It automatically loads all keys in ${HOME}/.ssh/ (non recursively).
    # Keys in subfolders must be added manualy with ssh-add.
    if [ ! -S "$SSH_AUTH_SOCK" ]; then
        eval `ssh-agent`
    fi
    # for tmux to use the ssh-agent the socket path should always be the same.
    # So we create a symlink to ensure the path will always be the same.
    if [ ! -S "${HOME}/.ssh/ssh_auth_sock" ]; then
        ln -sf $SSH_AUTH_SOCK ${HOME}/.ssh/ssh_auth_sock
    fi
}

function environment(){
    export ENVIRONMENT=$1
    if [ -e "${HOME}/.envrc" ]; then
        direnv reload
    else
        echo "Missing ${HOME}/.envrc"
    fi
}

function cluster(){
    export K8S_CLUSTER=$1
    if [ -e "${HOME}/.envrc" ]; then
        direnv reload
    else
        echo "Missing ${HOME}/.envrc"
    fi
}

function namespace(){
    export K8S_NAMESPACE=$1
    kubectl config set-context --current --namespace="${K8S_NAMESPACE}"
}

# Environments
function sandbox(){
    environment "sandbox"
}

# Clusters
function sandbox_gra(){
    environment "sandbox"
    cluster "k8s-gra7-601"
    namespace "default"
}
function sandbox_sbg(){
    environment "sandbox"
    cluster "k8s-test-sbg"
    namespace "default"
}
# function production-002(){
#     export ENVIRONMENT="jungle"
#     export GCP_PROJECT=jungle-181007
#     cluster "production-002"
# }
# function jungle-01(){
#     export ENVIRONMENT="jungle"
#     export GCP_PROJECT=jungle-181007
#     cluster "jungle-01"
# }

function b64d(){
    base64 -d <(echo "${1}")
}

function b64e(){
    base64 <(echo "${1}")
}

_fzf_complete_git() {
  _fzf_complete --multi --reverse --prompt="branch> " -- "$@" < <(
    git branch --all --format='%(refname:lstrip=-1)' --color --sort=-committerdate
  )
}
complete -F _fzf_complete_git git

# Personal configuration files
---------------------------------------

* tmux.conf : configuration file for [tmux](http://tmux.sourceforge.net/)
* .env: env file to be used by [autoenv](https://github.com/kennethreitz/autoenv)
